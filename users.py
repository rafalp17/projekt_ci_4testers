class User:
    def __init__(self, email, password):
        self.email = email
        self.password = password
        self.logged_in = False

    def login(self, password):
        if password == self.password:
            self.logged_in = True

    def logout(self):
        self.logged_in = False

        